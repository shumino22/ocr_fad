package com.naat.fad.ocrcomponent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity{

    private static int version = 0;

    public static native String salt(String path,String finalPath, long address,  String outputName);

    static {
        System.loadLibrary("native-lib");
    }

    public static String getOCRType(byte[] bytes, Camera camera, String path, String finalPath, String outputName){
        Camera.Size pictureSize = camera.getParameters().getPreviewSize();
        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        String result = "";

        Mat yuv = new Mat(new Size(pictureSize.width, pictureSize.height * 1.5), CvType.CV_8UC1);
        yuv.put(0, 0, bytes);

        Mat mat = new Mat(new Size(pictureSize.width, pictureSize.height), CvType.CV_8UC3);
        Imgproc.cvtColor(yuv, mat, Imgproc.COLOR_YUV2BGR_NV21, 3);

        result = salt(Environment.getExternalStorageDirectory().toString() + "/" + "fad_test" +  "/" ,Environment.getExternalStorageDirectory().toString() + "/" + "fad_test" +  "/documentosFAD/", mat.nativeObj, "cropINE" + version);

        return null;
    }
}


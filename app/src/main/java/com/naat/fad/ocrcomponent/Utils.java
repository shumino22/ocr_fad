package com.naat.fad.ocrcomponent;

import android.content.Context;

import java.io.File;

public class Utils {

    public static String getDocsPath(String name, Context ac) {
        String extr = Utils.getAllPAthRoot(ac);
        File dir = new File(extr);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return extr + name;
    }


    public static String getAllPAthRoot(Context ac)
    {
        String root =  ac.getFilesDir().getPath();

        //root =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();

        return root;
    }
}

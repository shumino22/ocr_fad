/* 	File		: 	idclassifierback.h
    Version 	:	0.5
    Description	: 	Protoype of ID Locator algorithm
    Date:		:	Aug 08, 2018
    Author		:	Ricardo Acevedo

*/

#ifndef IDCLASSIFIERBACK_H
#define IDCLASSIFIERBACK_H

#include <opencv2/opencv.hpp>

struct idClassifierResults
{
    std::string idClass;
    cv::Rect resultBox;
    cv::Mat resultImg;
};

idClassifierResults idClassifierBack( cv::Mat inputImage, std::string filePath = "-", std::string idType = "-", bool verbose = false );

#endif // IDCLASSIFIERBACK_H

/* 	File		: 	idclassifier.h
    Version 	:	1.0
    Description	: 	Protoype of ID Locator algorithm
    Date:		:	Jun 25, 2018
    Author		:	Ricardo Acevedo

*/

#ifndef IDCLASSIFIER_H
#define IDCLASSIFIER_H

#include <opencv2/opencv.hpp>

struct idClassifierResults
{
    std::string idClass;
    cv::Rect resultBox;
    cv::Mat resultImg;
};

idClassifierResults idClassifier( cv::Mat inputImage, std::string filePath = "-", bool verbose = false );

#endif // IDCLASSIFIER_H

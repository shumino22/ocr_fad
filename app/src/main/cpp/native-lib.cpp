#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include "idclassifier.h"
#include "idlocator.h"
#include <android/log.h>

using namespace std;
using namespace cv;

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}

extern "C" JNIEXPORT jstring
JNICALL Java_com_naat_fad_ocrcomponent_MainActivity_salt(JNIEnv *env, jobject ,
                                                                      jstring path,
                                                                      jstring finalPath,
                                                                      jlong  addrInputImage,
                                                                      jstring outImageName) {
    static const float ver = 0.5;
    static const bool showDebugInfo = true;

    std::cout <<"Id Locator Version: "<<ver << '\n' << std::endl;
    // read input image:
    std::string imagePath = jstring2string(env, path);
    std::string imageFinalPath = jstring2string(env, finalPath);
    std::string outputName = jstring2string(env, outImageName);
    std::cout << "Enter images folder path: ";
    std::cin >> imagePath;
    std::cout << "Enter output name [exclude extension .png]: ";
    std::cin >> outputName;
    std::cout << std::endl;

    cv::Mat& inputImage = *(cv::Mat*)addrInputImage;
    //cv::Mat inputImage = cv::imread( imagePath+"/test_ine.png");
    //cv::Mat inputImage = cv::imread( imagePath+"/entrada_test.png");

    if( inputImage.empty() )
    {
        return env->NewStringUTF("Can not load input image. (Check image path)");
    }


    static const int resizedWidthInput = 640;
    static const int resizedHeightInput = 480;
    cv::Mat inputResizedHD( cv::Size( resizedWidthInput, resizedHeightInput ), CV_8UC3, cv::Scalar(0) );
    cv::resize(inputImage, inputResizedHD, inputResizedHD.size(), 0, 0, cv::INTER_AREA);

    // cv::Mat inputImage = myuv;

    //deep copy of input for further processing (640 x 480):
    cv::Mat originalImage = inputImage.clone();

    static const int resizedWidth = 320;
    static const int resizedHeight = 240;

    int resizeFactor = originalImage.cols/resizedWidth;

    std::cout<<"resizeFactor: "<<resizeFactor<<std::endl;

    //resize the image:
    cv::Mat inputResized( cv::Size( resizedWidth, resizedHeight ), CV_8UC3, cv::Scalar(0) );
    cv::resize(inputResizedHD, inputResized, inputResized.size(), 0, 0, cv::INTER_AREA);

    //cv::Mat copyOfInputImage = inputResized.clone();

    //pass image to id locator, it will return a "detection code"
    //prototype:  idLocator( pathToImage, showDebugInfo )
    int detectionCode = idLocator( inputResized, showDebugInfo );



    //Let's see what we've got:
    switch (detectionCode) {
        case 0:;
            return env->NewStringUTF("Shit! Error!");
        case 1:
            try{
                idClassifierResults idClassfication = idClassifier( inputResizedHD, imagePath, showDebugInfo );
                std::cout<<"classification result: "<<idClassfication.idClass<<"\n"<<std::endl;

                //compression parameters:
                std::string finalResult = idClassfication.idClass;
                if (idClassfication.idClass.compare("Unknown/No Data Unknown/No Data") != 0){
                    __android_log_print(ANDROID_LOG_INFO, "resultImage", "Ingreso a generar imagen");
                    std::vector<int> compression_params;
                    compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
                    compression_params.push_back(0);

                    //write output:
                    cv::imwrite(imageFinalPath+outputName+".png",idClassfication.resultImg,compression_params);
                    return env->NewStringUTF("ID Found.Success you are la mera verga");
                }
            }catch (const char* msg) {
                return env->NewStringUTF("ID Found.But has error");
            }
            return env->NewStringUTF("ID Found.Pura de arabe");
        case 2:
            return env->NewStringUTF("No Valid Data (Check Alignment).");
        case 3:
            return env->NewStringUTF("No Valid Data (No ID).");
    }



    if (!(detectionCode == 1)){

        cv::waitKey();
        return env->NewStringUTF("Error");

    }

    //try to classify the image:


        cv::waitKey();
    return env->NewStringUTF("Error");
    }


/* 	File		: 	idlocator.h
    Version 	:	1.0
    Description	: 	Protoype of ID Locator algorithm
    Date:		:	Jul 03, 2018
    Author		:	Ricardo Acevedo

*/

#ifndef IDLOCATOR_H
#define IDLOCATOR_H

#include <opencv2/opencv.hpp>
float idLocator( cv::Mat inputImage, bool verbose = false );

#endif // IDLOCATOR_H

/*  File        :   idClassifierBack.cpp
    Version     :   0.5
    Description :   ID Classifier (Back)
    Date:       :   Aug 08, 2018
    Author      :   Ricardo Acevedo

*/

#include <iostream>
#include <string>
#include "ipfunctions.h"
#include "mlfunctions.h"

// RGB pixel definition:
typedef cv::Point3_<uint8_t> rgbPixel;

//structure of id classifier results:
struct idClassifierResults
{
    std::string idClass;
    cv::Rect resultBox;
    cv::Mat resultImg;
};

idClassifierResults idClassifierBack( cv::Mat inputImage, std::string filePath, std::string idType = "-", bool verbose = false ){

    //check for image path:
    if( !filePath.compare("-")  ){
        throw  "idClassifierBack>> Where's my image file path, my dude?";
    }

    cv::resize(inputImage,inputImage,cv::Size(320,240),0,0,cv::INTER_AREA);

    cv::Mat originalImage = inputImage.clone();


    bool imgCheck = inputImage.isContinuous();
    //prepare the gray and the Normalized RGB
    int imgChannels = inputImage.channels();
    //version of the input image:
    cv::Mat normalizedRGB, grayImage;

    //deep copy of input image:
    normalizedRGB = inputImage.clone();

    //accelerated normalized RGB conversion:
    normalizedRGB.forEach<rgbPixel>(&rgbNormalized);


    //define marker (quad) centers:
    std::vector<cv::Point> markerCenters;
    markerCenters.push_back( cv::Point(39,44) );
    markerCenters.push_back( cv::Point(279,44) );
    markerCenters.push_back( cv::Point(39,194) );
    markerCenters.push_back( cv::Point(279,194) );

    //Subsample window dimensions definition:
    static const int sampleWinHeight = 35;
    static const int sampleWinWidth = 35;

    //define sample quads coordinates:
    std::vector<cv::Point> sampleQuadCoordinates;
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[0].x - sampleWinWidth, markerCenters[0].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[1].x, markerCenters[1].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[2].x - sampleWinWidth, markerCenters[2].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[3].x, markerCenters[3].y) );

    //Classification dimensions window:
    static const int classWinHeight = 15;
    static const int classWinWidth = 15;

    //define classification quads coordinates:
    std::vector<cv::Point> classificationQuadCoordinates;
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[0].x, markerCenters[0].y) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[1].x - classWinWidth, markerCenters[1].y) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[2].x, markerCenters[2].y - classWinHeight) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[3].x - classWinWidth, markerCenters[3].y - classWinHeight) );

    //total quads on image:
    static const int totalQuads = 4;
    //decimate factor for sampling:
    static const int sampleDecimateFactor = 1;

    //std devs for each color are stored here, this is a flat vector.
    //I neeed this format because I'll compute the median of all channels at once:
    std::vector<float> stdVector ( 3*totalQuads, 0 );
    //std::vector<float> stdVectorAverage ( totalQuads, 0 );
    std::vector<float> meanVector ( 3*totalQuads, 0 );

    //RBG vartiation coeficients for each quad are stored here:
    std::vector<float> varCoeffVector ( 3*totalQuads, 0 );

    //valid pixels per quad go in this vector:
    std::vector<float> validPixelsVector ( 3*totalQuads, 0 );

    //scalars for image mean and image stdDev:
    cv::Scalar imageMean, imageStdDev;

    //sample window RGB matrix:
    cv::Mat sampleWindow = sampleWindow.zeros(sampleWinHeight, sampleWinWidth, CV_8UC3);

    //the quad counter:
    int quadCount = 0;

    /*
    **************
    Quad Sampling
    **************
    */

    //Loop thru all 4 defined quads:
    for ( int currentQuad = quadCount; currentQuad < totalQuads; ++currentQuad )
    {

        //start of the current quad in x:
        int quadColStart = sampleQuadCoordinates[currentQuad].x;
        //end of the current quad in x:
        int quadColEnd = quadColStart + sampleWinWidth;

        //start of the current quad in y:
        int quadRowStart = sampleQuadCoordinates[currentQuad].y;
        //end of the current quad in y:
        int quadRowEnd = quadRowStart + sampleWinHeight;

        //row index for target image (sampleWindow):
        int rowIndex = 0;

        //loop thru each quad's row:
        int rowEnd = quadRowEnd - sampleDecimateFactor;
        for ( int j = quadRowStart; j <= rowEnd; j = j + sampleDecimateFactor )
        {
             //col index for target image (sampleWindow):
            int colIndex = 0;

            //since this is a pixel-address approach, I need the current pixel of the sampled image
            //and the target pixel in the sample window.
            //I'll create a new "image" consisting of the sampled portion of the image
            //(the sample window):
            cv::Vec3b* currentPixel = normalizedRGB.ptr<cv::Vec3b>(j);
            cv::Vec3b* targetPixel = sampleWindow.ptr<cv::Vec3b>(rowIndex);

            //loop thru each quad's column:
            int colEnd = quadColEnd - sampleDecimateFactor;
            for ( int i = quadColStart; i <= colEnd; i = i + sampleDecimateFactor )
            {
                //fill the subwindow pixels with input image's
                targetPixel[colIndex] = currentPixel[i];
                colIndex++;
            }
            rowIndex++;
        }

        //compute and store statistics in vector:
        cv::meanStdDev(sampleWindow, imageMean, imageStdDev);

        // stdVectorIndex for storing my values:
        int stdVectorIndex = 3 * currentQuad;
        int decrementIndex = 2; // 3 colors: RGB

        //in each loop iteration, the vector stores 3 values,
        //loop thru each color (RGB):
        for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
        {
            //opencv operates RGB images as BGR (WTF?):
            stdVector[ stdVectorIndex + colorIndex ] =  imageStdDev[decrementIndex];
            meanVector[ stdVectorIndex + colorIndex ] = imageMean[decrementIndex];
            varCoeffVector[ stdVectorIndex + colorIndex ] = pow( imageStdDev[decrementIndex],2 )/imageMean[decrementIndex];

            decrementIndex--;
        }

    }

    static const int classDecimateFactor = 2;

    //target color of the object of interest:
    std::vector<int> targetColor{ 84, 84, 84 }; //rgb

    std::vector<int> markedPixelsVector (totalQuads,0);

    /*
    *******************
    Quad Classification
    *******************
    */
    int rgbSimilarBg;
    //Quad processing (Pixel Classification for each quad):
    for (int currentQuad = quadCount; currentQuad < totalQuads; ++currentQuad)
    {
        //start of the current quad in x:
        int quadColStart = classificationQuadCoordinates[currentQuad].x;
        //end of the current quad in x:
        int quadColEnd = quadColStart + classWinWidth;

        //start of the current quad in y:
        int quadRowStart = classificationQuadCoordinates[currentQuad].y;
        //end of the current quad in y:
        int quadRowEnd = quadRowStart + classWinHeight;

        int pixelCount = 0;

        //loop thru each quad's row:
        int rowEnd = quadRowEnd - classDecimateFactor;
        for ( int j = quadRowStart; j <= rowEnd; j = j + classDecimateFactor )
        {
            //read current pixel row:
            cv::Vec3b* currentPixel = normalizedRGB.ptr<cv::Vec3b>(j);

            //loop thru each quad's column:
            int colEnd = quadColEnd - classDecimateFactor;
            for ( int i = quadColStart; i <= colEnd; i = i + classDecimateFactor )
            {

                //rgb similarity counter:
                rgbSimilarBg = 0;

                //background and target same color check:
                for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
                {

                    int stdVectorIndex = 3*currentQuad + colorIndex;

                    //current mean:
                    float meanValue = floor( meanVector[stdVectorIndex] );

                    //differece between current mean and the target color:
                    float meanTargetDifference = abs( meanValue - targetColor[colorIndex] );
                    float varCoef = varCoeffVector[stdVectorIndex];
                    //checek against a minimum difference:
                    if (
                            ( (meanTargetDifference < 5) )//&& (redVarCoeff > 0.05) )  //varCoef >= 0.005 condition 1: similar BG color to target
                            //|| ( (meanTargetDifference < 2) && (redVarCoeff >= 0.05) )                      //condition 2: very similar BG color to target
                       ) {
                        //if true, current color is similar to target color:
                        rgbSimilarBg++;
                    }
                }

                //std::cout<<"currentQuad: "<<currentQuad<<" rgbSimilarBg: "<<rgbSimilarBg<<std::endl;

                //RGB check value:
                int colorCheck = 0;

                //loop thru each color (RGB):
                for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
                {

                    int stdVectorIndex = 3*currentQuad + colorIndex;

                    //current variation coefficient:
                    float variationCoefficient = varCoeffVector[stdVectorIndex];
                    //Std dev Thresh:
                    float stdThresh;

                    //store number of valid pixels in vector
                    if (rgbSimilarBg == 3 ){
                        validPixelsVector[currentQuad] = 0.3;
                    }else{
                        validPixelsVector[currentQuad] = 0.4;
                    }

                    //Assing std thresh according to variation coefficient:
                    if ( (variationCoefficient > 1) || (rgbSimilarBg == 3) ){
                        //high dispersion found:
                        stdThresh = 0.5;
                    }
                    else{
                        stdThresh = 3.3;
                    }

                    //current statistics:
                    float meanValue = floor( meanVector[stdVectorIndex] );
                    float stdDevValue = stdThresh * stdVector[stdVectorIndex];
                    float meanUpperLimit = floor( meanValue + stdDevValue );
                    float meanLowerLimit = floor( meanValue - stdDevValue );

                    //current pixel (just one color) to int conversion:
                    float currentColor = (float)currentPixel[i][2-colorIndex];

                    //"voting" system
                    //each color "votes" if its value is outside the expected stats,
                    //3 is the "heaviest" weight
                    //if the current pixel falls outside the expected range:
                    if ( (currentColor <= meanLowerLimit) || (currentColor >= meanUpperLimit) )
                    {
                        int stdDevFactor;
                        if (stdThresh == 0.5){
                            stdDevFactor = 1.8;
                        }else{
                            stdDevFactor = 4;
                        }

                        //check out the difference between current color and mean:
                        float colorDifference = abs( currentColor - meanValue );
                        //maximum Std Dev value to be consider a new color:
                        float maxStdDevFactor = stdDevFactor * stdVector[stdVectorIndex];
                        //see if the difference is greater than the expected:
                        if ( colorDifference > maxStdDevFactor )
                        {
                            colorCheck = 3; //yes, there's difference, vote with the "heaviest" weight

                        }else{

                            //only count colors different from the target
                            //float targetColorDifference = abs( currentColor - targetColor[colorIndex] );
                            if ( colorDifference > 0 ){
                                colorCheck++; //just a weight of 1
                            }
                        }
                    }
                }

                //after checking the 3 channels, check how many colors where different:
                if ( colorCheck >= 3 )
                {
                    //lt's draw some info:
                    if(rgbSimilarBg >= 3){
                        //foreground object with a bg similar to target color:
                        cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(17,172,245) );
                    }
                    else{
                        //foreground object with a bg different from target color:
                        cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(0,0,255) );
                    }

                    //pixels matched so far (for each marker/quad):
                    pixelCount++;

                }else{
                    //no object detected:
                    cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(255,0,0) );
                }
            }

        }

        //Store number of pixels checked
        //this vector stores the total "voters" per quad:
        markedPixelsVector[currentQuad] = pixelCount;
    }

    //Result analysis:

    //total of pixels analyzed per quad:
    float classWinPixels = classWinHeight * classWinWidth * 1/( pow(classDecimateFactor,2) );

    //check markedPixelsVector for valid foreground object:
    int markerCount = 0 ;

    bool q1 = false;
    bool q2 = false;
    bool q3 = false;
    bool q0 = false;

    for (int i = 0; i < totalQuads; ++i)
    {
        int redColor = 255; //not enough foreground pixels detected (red)
        int greenColor = 0; //enough foreground pixels detected (green)

        // percent of the pixels in window that must defined a "foreing object":
        float validPixels = floor( validPixelsVector[i] * classWinPixels );
        //std::cout<<"Quad: "<<i<<" Valid Pixels %: "<<validPixelsVector[i]<<" Valid Pixels: "<<validPixels<<std::endl;

        //check pixel votes, the number in the vector must equal
        //the number of valid pixels:
        if ( markedPixelsVector[i] >= validPixels ) //enough foreground pixels
        {
            //a foreing object is found in the picture
            markerCount++;
            redColor = 0;
            greenColor = 255;

            // no ID Function: determine if an object is found in the picture:
            switch (i)
            {
                case 0:
                        q0 = true;
                        break;
                case 1:
                        q1 = true;
                        break;
                case 2:
                        q2 = true;
                        break;
                case 3:
                        q3 = true;
                        break;
            }

        }
        //draw rectangle with results: green -> foreign object detected in quad, red-> no detection
        cv::rectangle( inputImage, classificationQuadCoordinates[i], classificationQuadCoordinates[i] + cv::Point(classWinWidth, classWinHeight), cv::Scalar(0,greenColor,redColor));
    }

    bool noIdFunction = (q0 && q3 && !q1 && !q2) || (q1 && q2 && !q0 && !q3) ;

    //this is the detection code that the module returns:
    //3 -> No ID present in the image
    //1 -> Aligned ID found
    //2 -> ID found but no aligned

    if (verbose){
        std::cout<<"idLocator(Back)>> markerCount: "<<markerCount<<" noIdFunction: "<<noIdFunction<<std::endl;
    }

    int detectionCode = 0;

    if ( markerCount == 4 ){
        detectionCode = 1;
    }else if ( (noIdFunction == 1) || (markerCount <= 1)  ){
        detectionCode = 3;
    }else{
        detectionCode = 2;
    }

    std::cout<<"idLocator(Back)>> Detection Code is: "<<detectionCode<<std::endl;

    //check if ID is present and aligned:
    if (detectionCode != 1){

        throw "idLocator(Back)>> No back /misaligner ID. Check for valid data or alignment. detectionCode: ";
    }
    //crop image:

    cv::Rect cropRectangle;
    cropRectangle.x = markerCenters[0].x;
    cropRectangle.y = markerCenters[0].y;
    cropRectangle.width = markerCenters[3].x - cropRectangle.x;
    cropRectangle.height = markerCenters[3].y - cropRectangle.y;

    inputImage = originalImage(cropRectangle);

    /*
    ************************
    ID (Back) Classification
    ************************
    */

    //everything so far is ok, check for ID-specific characteristics:

    //define the back roi as function of input ID:
    cv::Rect backROI;

    bool runTemplateMatch = false;

    int thresholdingWindowSize = 0;
    int thresholdingConstant = 0;

    //check front string:

    if ( (!idType.compare("01 (INE)") || !idType.compare("03 (IFE)") ) ){

        //roi for idType = 01 INE, 03 IFE:
        backROI.width = 50;
        backROI.height = 15;

        backROI.x = inputImage.cols - backROI.width;
        backROI.y = 0;

        //run template match:
        runTemplateMatch= true;

        //values for thresholding:
        thresholdingWindowSize = 9;
        thresholdingConstant = 15;

    }else if( !idType.compare("02 (IFE)") ){

        //roi for idType = 01 INE, 03 IFE:
        backROI.x = 65;
        backROI.y = 10;
        backROI.width = 150;
        backROI.height = 6;

        //values for thresholding:
        thresholdingWindowSize = 7;
        thresholdingConstant = 10;

    }else{ //show's over:

        throw "idClassifier(Back)>>Error: ID not supported.";

    }

    std::string idBackType = "-1 (Unknown)";

    if(runTemplateMatch){

        std::cout<<"idClassifier(Back)>> Input image is "+idType+" searching for Red OVI..."<<std::endl;

        //crop normalized input:

        normalizedRGB = normalizedRGB(cropRectangle);

        //crop ROI:
        normalizedRGB = normalizedRGB(backROI);

        std::vector<cv::Mat> redChannel;   //destination array
        //extract red channel:
        cv::split( normalizedRGB, redChannel );//split source

        //scalars for gray mean and gray stdDev:
        cv::Scalar tempMean, tempStdDev;

        //compute gray mean/stdDev of the gray input image:
        cv::meanStdDev(redChannel[2], tempMean, tempStdDev);

        //check for red mean, similar to red target value:
        static const float minMean = targetColor[0]+2;
        static const float maxMean = targetColor[0]+10;

        if( (tempMean[0]>minMean) && (tempMean[0]<maxMean) ){

            //check for std dev value:
            static const float minStdDev = 1;
            static const float maxStdDev = 10;

            if( (tempStdDev[0]>minStdDev) && (tempStdDev[0]<maxStdDev) ){

                std::cout<<"idClassifier(Back)>> Std dev is above expected value, seems there's a foreign object in the image."<<std::endl;

            }else{

                throw "idClassifier(Back)>> Std dev is below expected value, nothing of interest is on the image.";

            }

        }else{

            throw "idClassifier(Back)>> Nothing of interest here... (No red elements). ";

        }

        //let's continue processing:

        //backRoi RGB2Gray conversion:
        cv::Mat grayBackRoi;
        cv::cvtColor(normalizedRGB, grayBackRoi, cv::COLOR_RGB2GRAY);

        //threshold the header ROI:
        double th = cv::threshold(grayBackRoi, grayBackRoi, 0,255,cv::THRESH_OTSU);

        //invert the image for matching process:
        grayBackRoi =  255 - grayBackRoi;


        //cool, let's run the matching process:

        //load back templates:
        cv::Mat template02 = cv::imread( filePath + "reverseIfe01_template.png" );
        cv::Mat template01 = cv::imread( filePath + "reverseIne01_template.png" );

        //vector of template mats:
        std::vector<cv::Mat> templateVector;
        templateVector.push_back(template01);
        templateVector.push_back(template02);

        //vector of types:
        std::vector<std::string> templateType;
        templateType.push_back("11 (INE - E,F)"); //INE E,F back
        templateType.push_back("13 (IFE - D)"); //IFE D back

        //select the matching method:
        static const int matchMethod = CV_TM_CCOEFF_NORMED;

        //bestMatch variable to hold historic best match
        double bestMatch = 0;
        //store the bestMatch X,Y point on the image
        cv::Point bestMatchPoint;

        int bestTemplateCols; int bestTemplateRows;

        //minimum allowable match value:
        static const float minMatchValue = 0.5;

        //begin template match processing:
        for (int i = 0; i < (int)templateVector.size(); ++i)
        {
            //get the current template:
            cv::Mat currentTemplate;

            currentTemplate = templateVector[i];

            if ( currentTemplate.empty() ){
                throw "idClassifier(Back)>> Error loading template  (check image path).\n";
            }

            //rgb2gray conversion:
            cv::cvtColor(currentTemplate, currentTemplate, cv::COLOR_RGB2GRAY);


            //mat used to store template match results:
            cv::Mat matchResult;
            int resultCols =  grayBackRoi.cols - currentTemplate.cols + 1;
            int resultRow = grayBackRoi.rows - currentTemplate.rows + 1;

            //std::cout<<"idClassifier(Back)>> resultCols: "<<resultCols<<std::endl;
            //std::cout<<"idClassifier(Back)>> resultRow: "<<resultRow<<std::endl;

            matchResult.create( resultRow, resultCols, CV_32FC1 );

            //apply the matching algorithm:
            cv::matchTemplate( grayBackRoi, currentTemplate, matchResult, matchMethod );

            double minVal; double maxVal; double matchVal;
            cv::Point minLoc; cv::Point maxLoc; cv::Point matchPoint;

            //get the location of the best match:
            cv::minMaxLoc( matchResult, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );

            //for the first two methods ( TM_SQDIFF and MT_SQDIFF_NORMED ) the best match are the lowest values.
            //for all the others, higher values represent better matches

            if( matchMethod  == cv::TM_SQDIFF || matchMethod == cv::TM_SQDIFF_NORMED )
            {
                matchPoint = minLoc;
                matchVal = 1-minVal;

            }else{
                matchPoint = maxLoc;
                matchVal = maxVal;
            }


            //bool foundMatch = false;

            //let's see if the match value is above minimum and if it is the best match so far:
            if ( (matchVal > minMatchValue) && (matchVal > bestMatch) ){

                //store all the best match values:
                bestMatch = matchVal;
                bestMatchPoint = matchPoint;
                idBackType = templateType[i];
                //idSubType = templateSubtype[i];

                bestTemplateCols = currentTemplate.cols;
                bestTemplateRows = currentTemplate.rows;
            }

        }

        //it's done:
        if(bestMatch == 0){
            //no match found:
            throw "idClassifier(Back)>> Error: Expected "+idType+" type ID, but I didn't found a match.\n";
        }else{
            std::cout<<"idClassifier(Back)>> OVI found."<<std::endl;
            cv::rectangle( inputImage, cv::Point( backROI.x + bestMatchPoint.x, backROI.y + bestMatchPoint.y), cv::Point( backROI.x + bestMatchPoint.x + bestTemplateCols , backROI.y + bestMatchPoint.y + bestTemplateRows), cv::Scalar(0,0,255), 1, 8, 0 );
        }

    }else{

        std::cout<<"idClassifier(Back)>> Input image is "+idType+" searching for code strip..."<<std::endl;

        //crop ROI: (Code Strip)
        cv::Mat grayBackRoi;
        grayBackRoi = inputImage(backROI);

        //scalars for image mean and image stdDev:
        cv::Scalar imageMean, imageStdDev;

        //backRoi RGB2Gray conversion:

        cv::cvtColor(grayBackRoi, grayBackRoi, cv::COLOR_RGB2GRAY);

        if (verbose){
            cv::namedWindow( "ID (ROI Gray) " );
            cv::imshow("ID (ROI Gray) ", grayBackRoi);
        }

        //apply brightness/contrast adjustment to enchance blacks & whites:
        contrastBrightnessAdjustment(grayBackRoi,1.8,-90);

        //get otsu's threshold base value:
        cv::Mat grayBackRoiClone;
        double otsuThresh = cv::threshold(grayBackRoi,grayBackRoiClone,0,255,cv::THRESH_OTSU);


        //let's apply a value a little bit below otsu's:
        otsuThresh = floor( (1-0.05)*otsuThresh );

        cv::threshold(grayBackRoi,grayBackRoi,otsuThresh,255,cv::THRESH_BINARY);


        //calculate the image's signal-to-noise ratio:
        double SNR = getPSNR(grayBackRoi);

        //compute std dev of the code strip (ife id):
        cv::meanStdDev(grayBackRoi, imageMean, imageStdDev);

        //minimum/maximum values:

        //static const int minMean = 100;
        //static const int maxMean = 200;
        static const int minStdDev = 110;

        static const float minSNR = 2;
        static const float maxSNR = 7;

        //variables for rectangle color:
        int greenColor = 0;
        int redColor = 255;

        //coefVar = std( double(imageGray(:)) )^2/mean2( double(imageGray) );

//        float coefVar = 0;
//        if ( imageMean[0] != 0 ){
//            coefVar = pow(imageStdDev[0],2)/imageMean[0];
//        }

        //std::cout<<"idClassifier(Back)>> coefVar: "+std::to_string(coefVar)<<std::endl;

        //if( (imageMean[0]>minMean)&&(imageMean[0]<maxMean)&&(imageStdDev[0]>minStdDev) ){
        //if ( (coefVar>70)&&(coefVar<150) ){

        //check for statistics denoting the white-black code strip:
        if ( (SNR > minSNR) && (SNR < maxSNR) && (imageStdDev[0] > minStdDev) ){

            std::cout<<"idClassifier(Back)>> Code strip found."<<std::endl;
            idBackType = "12 (IFE- C)";
            greenColor = 255;
            redColor = 0;

        }else{

            //match not found
            std::cout<<"idClassifier(Back)>> Error: Expected "+idType+" but I didn't found a code strip match."<<std::endl;
            std::cout<<"idClassifier(Back)>> ID mismatch/ID not valid?\n"<<std::endl;
            idBackType = "-2 (INV/MM)";
        }
        //show rectangle around strip sampling area:
    }

    //prepare a black textbox:
    cv::Rect textBox;
    textBox.x = inputImage.cols - 80;
    textBox.y = inputImage.rows - 10;
    textBox.width = inputImage.cols - textBox.x;
    textBox.height = inputImage.rows - textBox.y;

    //draw a black textbox:
    cv::rectangle( inputImage,textBox,cv::Scalar(0),CV_FILLED,cv::LINE_8,0);
    //put text (id type + subtype):
    cv::putText( inputImage, idBackType, cv::Point(textBox.x+1,inputImage.rows-2),cv::FONT_HERSHEY_SIMPLEX,0.3,cv::Scalar(0,255,0),1,cv::LINE_8,false );

    //show me the goods:

    std::cout<<"idClassifier(Back)>> Front type: "<<idType<<std::endl;
    std::cout<<"idClassifier(Back)>> Back type: "<<idBackType<<std::endl;

    std::string frontCode = idType.substr (1,1);
    std::string backCode = idBackType.substr (1,1);

    //front and back mismatch:
    if (frontCode.compare(backCode)){
        std::cout<<"idClassifier(Back)>> Warning, expected "+idType+" but found "<<idBackType<<"."<<std::endl;
        std::cout<<"idClassifier(Back)>> Check input image/front type.\n"<<std::endl;
    }else{
        std::cout<<"idClassifier(Back)>> ID front and back match.\n"<<std::endl;
    }

    //std::string finalResult = idType+" "+idSubType;

    //cv::waitKey();
    //return the final results, packed in the results struct:
    idClassifierResults functionResults;
    functionResults.idClass = idBackType;
    functionResults.resultBox = textBox;
    functionResults.resultImg = inputImage;

    return functionResults;
}

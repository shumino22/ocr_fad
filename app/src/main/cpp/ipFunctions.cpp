/* 	File		: 	plFunctions.cpp
    Version 	:	1.6
    Description	: 	Auxiliary functions for image processing algorithms
    Date:		:	Jul 03, 2018
    Author		:	Ricardo Acevedo

*/

#include <iostream>
#include <map> //for hash & dictionary structures
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <string>

//verbose configuration:
static bool verbose = false;

//verbose mode setter:
void setVerbose( bool newVerbose = false ){
    verbose = newVerbose;
    //std::cout <<"setVerbose> verbose set to: "<<verbose<<'\n'<<std::endl;
}

//structure of properties:
struct properties
{
    //bool verbose = false;
    float property01;
    float property02;
    float property03;
    float property04;
    float property05;
};

// typedef for dictionary of functions "extraProperties", functionPointer is an alias for float(*)(void)
// pointer to function with 3 arguments returning a structure of properties.
typedef properties (*functionPointer)(cv::Mat bwImage, float arg1, float arg2);

// function that computes perimeter and area from a binary (8-bit) image.
// two optional arguments can be specified.
// returns structure containing perimeter and area

properties computePerimeterArea( cv::Mat bwImage, float arg1 = 0 , float arg2 = 0 ){

    std::vector<std::vector<cv::Point>> contours;

    cv::findContours(bwImage, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE); // Retrieve only external contour

    float perimeter = (float)cv::arcLength(contours[0], true);
    float area = (float)cv::contourArea(contours[0]);

    if (verbose){
        std::cout <<"computePerimeterArea> computed perimeter: "<<perimeter<<'\n'<<std::endl;
        std::cout <<"computePerimeterArea> computed area: "<<area<<'\n'<<std::endl;
    }

    properties functionResults;
    functionResults.property01 = perimeter;
    functionResults.property02 = area;

    return functionResults;

}

// function that computes compactness from a binary (8-bit) image.
// two optional arguments can be specified (perimeter and area).
// returns structure containing the compactness area.

properties computeCompactness( cv::Mat bwImage, float perimeter = 0, float area = 0 ){

    if(!perimeter){
        if (verbose){
            std::cout <<"computeCompactness> perimeter not provided."<<'\n'<<std::endl;
        }
        perimeter = computePerimeterArea(bwImage).property01;
    }

    if(!area){
        if (verbose){
            std::cout <<"computeCompactness> area not provided."<<'\n'<<std::endl;
        }
        area = computePerimeterArea(bwImage).property02;
    }


    float compactness = (12.5664*area)/pow(perimeter, 2);

    if (verbose){
        std::cout <<"** computeCompactness> computed compactnes: "<<compactness<<'\n'<<std::endl;
    }

    properties functionResults;
    functionResults.property01 = compactness;

    return functionResults;
}


//structure of bounding box
//typical bb, {x,y} upper left coordinates
//{width, height} of the bbo
struct boundingBox
{
   int x;
   int y;
   int width;
   int height;
};

// function that computes boundingBox (composite) from a binary (8-bit) image
// with multiple blobs.
// prototype: computeBoundingBox( bwImage, address of bounding box struct )
// populates the target structure with the bounding box values

void computeBoundingBox( cv::Mat bwImage, boundingBox &boxData ){

    // Vector for storing found contours (the 2d points):
    std::vector< std::vector< cv::Point > > contours;
    // Vector for storing hierarchy information
    std::vector< cv::Vec4i > hierarchy;
    //find the contours:
    cv::Mat colorImage;
    cv::cvtColor(bwImage, colorImage, cv::COLOR_GRAY2RGB);
    cv::findContours( bwImage, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE );

    //the coordinates im looking for the composite bbox:
    int maxX = 0; // width of the curve + minimum x
    int minX = 300; // minimum x
    int minY = 300; // height of the curve + min y
    int maxY = 0; // min y

    cv::Rect contourRect;

    for( int i = 0; i < (int)contours.size(); i++ )
      {
        float blobArea = cv::contourArea(contours[i]);

        //outter most contours only:
        if (hierarchy[i][3] == -1) {

            //simple bounding box:
            contourRect = boundingRect(contours[i]);

            //get the coordinates of the composite bbox:
            int countourMaximumX = contourRect.x + contourRect.width;
            if ( countourMaximumX > maxX){

                maxX = countourMaximumX;
            }

            if (contourRect.x < minX){
                minX = contourRect.x;
            }

            int countourMaximumY = contourRect.y + contourRect.height;
            if (countourMaximumY > maxY){
                maxY = countourMaximumY;
            }

            if (contourRect.y < minY){
                minY = contourRect.y;
            }

        }

      }

    //width and height of the composite box:
    float width = maxX - minX;
    float height = maxY - minY;

    if (verbose){

        rectangle( colorImage, cv::Point(minX,minY), cv::Point(maxX, maxY), cv::Scalar(0,0,255));
        cv::imshow("Contours ", colorImage);
        std::cout<<"bbox info: "<<std::endl;
        std::cout<<"minX: "<<minX<<" minY:"<<minY<<" maxX: "<<maxX<<" maxY:"<<maxY<<std::endl;
        std::cout<<"width: "<<width<<" height:"<<height<<'\n'<<std::endl;
    }

    //populate the target struct with the computed values:;
    boxData.x = minX;
    boxData.y = minY;
    boxData.width = width;
    boxData.height = height;

}

// function that computes eccentricity from a binary (8-bit) image.
// it also computes the bounding box, necessary for eccentricity calculation.
// prototype: computeEccentricity( bwImage )
// returns the eccentricity property

properties computeEccentricity( cv::Mat bwImage, float arg1 = 0 , float arg2 = 0 ){

    boundingBox boundingBoxData;
    computeBoundingBox( bwImage, boundingBoxData );

    float width = (float)boundingBoxData.width;
    float height = (float)boundingBoxData.height;

    float eccentricity = height/width;
    properties functionResults;
    functionResults.property01 = eccentricity;

    if (verbose){
        std::cout<<"computeEccentricity> eccentricity: "<<eccentricity<<std::endl;
    }

    return functionResults;
}

// function that computes rectangularity from a binary (8-bit) image.
// prototype: computeRectangularity( bwImage, (optional)width, (optional)height )
// returns the rectangularity property
// if either width nor height are specified, these are computed using
// the bounding box function.

properties computeRectangularity( cv::Mat bwImage, float width = 0 , float height = 0 ){

    // Vector for storing found contours (the 2d points):
    std::vector< std::vector< cv::Point > > contours;
    // Vector for storing hierarchy information
    std::vector< cv::Vec4i > hierarchy;
    //find the contours:
    cv::findContours( bwImage, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE );
    cv::Rect contourRect;

    float totalArea = 0;

    for( int i = 0; i < (int)contours.size(); i++ )
    {
    //outter most contours only:
        if (hierarchy[i][3] == -1) {
            totalArea = totalArea + cv::contourArea(contours[i]);
        }
    }

    //width or height were not supplied:
    if ( (width == 0) | (height == 0) ){

        if (verbose){
            std::cout<<"computeRectangularity>  width or height were not supplied. "<<std::endl;
        }

        boundingBox boundingBoxData;
        computeBoundingBox( bwImage, boundingBoxData );

        width = (float)boundingBoxData.width;
        height = (float)boundingBoxData.height;
    }

    float rectangularity = totalArea/(width * height ); // rect. computed as: shape area / bbox area

    properties functionResults;
    functionResults.property01 = rectangularity;
    if (verbose){
        std::cout<<"computeRectangularity>  rectangularity: "<<rectangularity<<std::endl;
    }

    return functionResults;
}

//function that creates/initializes the dictionary function
//that points to auxiliary functions.
//The dictionary points to extra functions used to compute
//extra properties such as perimeter, area, compactness, etc.

std::map<std::string, functionPointer> initDictionary(){

    std::map<std::string, functionPointer> functionDictionary;

    //dictionary items ( key, value ):
    functionDictionary.emplace( "compactness", &computeCompactness );
    functionDictionary.emplace( "perimeterArea", &computePerimeterArea );
    functionDictionary.emplace( "eccentricity", &computeEccentricity );
    functionDictionary.emplace( "rectangularity", &computeRectangularity );

    if (verbose){
        std::cout << "initDictionary> map created once..." << '\n'<<std::endl;
    }

    return functionDictionary;
}

//function that searches a pointer (function address) given a key (propety string).
//Prototype: getExtraProperties(string Key, cv::Mat binaryImage float arg1 float arg2), returns structure
//containing the calculated property.

properties getExtraProperties(const std::string& pointerFunction, cv::Mat bwImage, float arg1 = 0, float arg2 = 0){

    // the dictionary will be initialized only at the first time that is called:
    static std::map<std::string, functionPointer> functionDictionary = initDictionary();

    //find the value (function), according to the given key:
    auto iter = functionDictionary.find(pointerFunction);

    //the value was not found:
    if (iter == functionDictionary.end()){
        throw "getExtraProperties> no property found.";
    }

    //the called function results are stored here:
    properties functionResults;
    //execute value (function) and store it in a float variable:
    functionResults = (*iter->second)(bwImage, arg1, arg2);

    //return the property variable:
    return functionResults;
}

// Function that counts number of holes in image.
// Prototype: countHoles(cv::Mat bwImage [8 bit binary image]) returns
// the number of holes as an int.

int countHoles(cv::Mat bwImage){

    cv::Mat colorImage;
    cv::cvtColor(bwImage, colorImage, cv::COLOR_GRAY2RGB);

    // Vector for storing found contours (the 2d points):
    std::vector< std::vector< cv::Point > > contours;
    // Vector for storing hierarchy information
    std::vector< cv::Vec4i > hierarchy;

    // the contour detection mode is CV_RETR_CCOMP
    // CV_RETR_CCOMP retrieves all the contours and organizes them into a two-level hierarchy,
    // where the top-level boundaries are external boundaries of the components and the
    // second-level boundaries are boundaries of the holes

    int holeCount = 0;
    // Find the contours in the image
    cv::findContours( bwImage, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE );

    // iterate through each contour in search for holes:
    for( int i = 0; i < (int)contours.size(); i++ )
      {
       cv::Rect contourRect = boundingRect(contours[i]);
        //if hierarchy[i][3] > -1, the contour (hole at level 2) its the only one to have parents
        if (hierarchy[i][3] > -1)
        {
        rectangle( colorImage, cv::Point(contourRect.x,contourRect.y), cv::Point(contourRect.x + contourRect.width, contourRect.y + contourRect.height), cv::Scalar(0,255,0));
           holeCount++;
           //std::cout<<"i: "<<i<<" X = "<< contourRect.x <<" Y = "<< contourRect.y  << std::endl;
           //std::cout<<"[0] = "<< hierarchy[i][0] <<" [1] = "<< hierarchy[i][1] <<" [2] = "<< hierarchy[i][2] <<" [3] = "<< hierarchy[i][3]<<  std::endl;
       }
      }

    if (verbose){
        rand();
        int rNum = rand() % 20 + 1;
        cv::imshow( "countHoles> Holes found ", colorImage );
    }

    //release my shit:
    colorImage.release();

    return holeCount;

}

//structure of countConnectedComponents' function (can be 2 types):
struct ccResults
{
   int cc; //number of connected components:
   cv::Mat filteredMat; //filtered Mat;
};

// Function that counts number of connected components in image.
// Wrapper for cv::connectedComponents().
// Prototype: countConnectedComponents(cv::Mat bwImage, int connectivity = 8, bool filterBlob = false,
// int minArea = 5000)
// returns the number of connected components as an int.
// Function receives 8-bit binary image, (optional) connectivity parameter, (optinal) flag to filter pixels
// based on minimum area and (optional) the minimum area.



// Returns the Euler number for the binary image BW.
// The Euler number is the total number of objects in the image
// minus the total number of holes in those objects
// Matlab: eul = bweuler(BW,conn)
// Prototype: eulerNumber(cv::Mat bwImage, int connectivity, int minArea) returns eulerNumber
// Function receives 8-bit binary image, (optional) connectivity parameter and (optional)
// minimum area to filter out.



// Function that removes isolated pixels from a binary (8-bit) input;
// Applies the "hit or miss" technique.
// Prototype: hitOrMiss ( cv::Mat bwImage ), returns a (8-bit) binary
// matrix  containing the filtered image.


// Function that approximates Matlab's Skeletonization function.
// Matlab: Skel = bwskel(BW1);
// Prototype: cv::Mat skeletonize( cv::Mat bwImage )
// receives a (8-bit) binary input image and returns a (8-bit)
// binary matrix containing the (approximate) skeleton of the input

cv::Mat skeletonize( cv::Mat bwImage ){

    //skel matrix:
    cv::Mat skel( bwImage.size(), CV_8UC1, cv::Scalar(0) );
    //temp matrices:
    cv::Mat temp;
    cv::Mat eroded;
    //cross-shaped 3x3 structuring element
    cv::Mat SE = cv::getStructuringElement( cv::MORPH_CROSS, cv::Size(3, 3) );
    //itertion flag:
    bool done;
    //skeletonization procedure:
    do
    {
        cv::erode(bwImage, eroded, SE);
        cv::dilate(eroded, temp, SE);
        cv::subtract(bwImage, temp, temp);
        cv::bitwise_or(skel, temp, skel);
        eroded.copyTo(bwImage);
        //yes, iterate til the end of time...
        done = (cv::countNonZero(bwImage) == 0);

    } while (!done);

    //remove isolated pixels:
    //skel = hitOrMiss( skel );

    // //close the shapes, if possible:
    // //note: this thickens the skeleton a couple of pixels...
    // SE = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(4,4) );
    // cv::dilate(skel,skel,SE);

    //release my shit:
    temp.release(); eroded.release(); SE.release();

    return skel;

}

//higher order statistics such as skewness, kurtosis, aSx, aSy, etc...
//the properties can be stored in the propreties structure...

properties extraStatistics( cv::Mat bwImage, bool isImage = true ){

    cv::Moments momentStructure = cv::moments(bwImage, isImage);

    float m00 = momentStructure.m00; //area
    float m01 = momentStructure.m01;
    float m10 = momentStructure.m10;
    float m11 = momentStructure.m11;
    float m12 = momentStructure.m12;

    float m02 = momentStructure.m02;
    float m20 = momentStructure.m20;
    float m21 = momentStructure.m20;

    float m03 = momentStructure.m03;
    float m30 = momentStructure.m30;

    //Intermidate calcs:

    float S1 = 2*m11;
    float S2 = m30 - 3*m12;

    float C1 = m20 - m02;
    float C2 = m03 - 3*m21;
    float C3 = pow(C1, 2) - pow(S1, 2);

    float S3 = 2*S1*C1;

    float I1 = pow(C1, 2) + pow(S1, 2);
    float I2 = pow(C2, 2) + pow(S2, 2);
    float I3 = m20 + m02;

    float inverseA = 1/pow(m00, 0.5);
    float K = I1 * pow(I3, 1.5) * inverseA;

    //The extra propeties:

    float px = I1 / pow(I3, 2);
    float py = m00 * ( I2 / pow(I3, 3) );

    float aSx = (C2 * C3 + S2 * S3) / K;
    float aSy = (S2 * C3 - C2 * S3) / K;

    //float skewnessX = m30/pow(m20, 1.5);
    //float skewnessY = m03/pow(m02, 1.5);

    //no kurtosis :'(

    properties functionResults;
    functionResults.property01 = px;
    functionResults.property02 = py;
    functionResults.property03 = aSx;
    functionResults.property04 = aSy;
    //functionResults.property05 = ;
    //functionResults.property06 = py;

    return functionResults;
}


// function that computes the crushed vector from a binary (8-bit) image.
// prototype: getCrushedVector( bwImage, outVector, scan mask start x, scan mask end x, scan mask start y, scan mask height )
// returns (modifies by reference) the crushed vector

void getCrushedVector( cv::Mat inputImage, std::vector<uchar>& crushedVector, int maskStartX = 0, int maskEndX = 0, int maskStartY = 0, int scanRows = 0 ){

    int vectorLength = maskEndX - maskStartX;

    //mah pointers:
    uchar* startPixel;
    uchar* endPixel;

    //loop thru the "rows":
    for (int j = 0; j < scanRows; ++j)
    {
        //pointer to the first pixel (row, col):
        startPixel = inputImage.ptr<uchar>(maskStartY+j,maskStartX);
        //pointer to the last pixel:
        endPixel = startPixel + vectorLength;

        int i = maskStartX;

        //vector index:
        int vectorIndex = 0;

        //loop thru them "colums":
        for (; startPixel != endPixel; startPixel++)
        {
          //std::cout<<"j: "<<j<<" i: "<<i<<" vectorIndex: "<<vectorIndex<<std::endl;

            if( *startPixel == 255 ){

                //get the pixel currently stored in the crushed vector:
                uchar crushPixel = crushedVector[vectorIndex];
                //xor current and stored pixel:
                uchar orResult = crushPixel | *startPixel;
                //fill the crushed vector with result at current position:
                crushedVector[vectorIndex] = orResult;
                //std::cout<<"orResult: "<<(int)crushedVector[vectorIndex]<<" at "<<vectorIndex<<"\n"<<std::endl;

            }

          vectorIndex++;
          i++;
        }
    }

}

// Accelerated function that computes RGB Normalized version of a RGB (24-bit) image.
// prototype: rgbNormalized( current pixel addres, pixel position )
// returns (modifies by reference) the input image

// RGB pixel definition:
typedef cv::Point3_<uint8_t> rgbPixel;

void rgbNormalized(rgbPixel &rgbPixel, const int *position){

    float r = (float)rgbPixel.z;
    float g = (float)rgbPixel.y;
    float b = (float)rgbPixel.x;

    float pixelSum = r + g + b;

    rgbPixel.x = (uchar)(255*b / pixelSum);
    rgbPixel.y = (uchar)(255*g / pixelSum);
    rgbPixel.z = (uchar)(255*r / pixelSum);

}

void rgbNormalizedNaive(cv::Mat &inputImage){

    cv::MatIterator_<cv::Vec3b> it, end;
    for (it = inputImage.begin<cv::Vec3b>(), end = inputImage.end<cv::Vec3b>(); it != end; ++it) {
        uchar &r = (*it)[2];
        uchar &g = (*it)[1];
        uchar &b = (*it)[0];
    // Modify r, g, b values
        float pixelSum = r + g + b;
        b = cv::saturate_cast<uchar>(255*b / pixelSum);
        g = cv::saturate_cast<uchar>(255*g / pixelSum);
        r = cv::saturate_cast<uchar>(255*r / pixelSum);
    }

}

void contrastBrightnessAdjustment( cv::Mat inputImage, float alpha, int beta ){

    cv::MatIterator_<cv::Vec3b> it, end;
    for (it = inputImage.begin<cv::Vec3b>(), end = inputImage.end<cv::Vec3b>(); it != end; ++it) {
        uchar &pixel = (*it)[0];
        pixel = cv::saturate_cast<uchar>(alpha*pixel+beta);

    }

}

double getPSNR( cv::Mat I1 )
{
    cv::Mat temp;
    //cv::absdiff(I1, I2, s1);       // |I1 - I2|
    I1.convertTo(temp, CV_32F);  // cannot make a square on 8 bits
    temp = temp.mul(temp);           // |I1 - I2|^2

    cv::Scalar s = cv::sum(temp);         // sum elements per channel

    double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

    if( sse <= 1e-10) // for small values return zero
        return 0;
    else
    {
        double mse = sse /(double)(I1.channels() * I1.total());
        double psnr = 10.0*log10((255*255)/mse);
        return psnr;
    }
}


/* 	File		: 	mlFunctions.cpp
	Version 	:	1.6
	Description	: 	Auxiliary functions for machine learning algorithms
    Date:		:	Jul 03, 2018
	Author		:	Ricardo Acevedo
	
*/

#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip> //format for output stream
#include <algorithm> //for sort function

//function that receives a float vector and computes its absolute norm.
//additionaly, a type of norm can be specified:
// mode = 0 -> L1, mode = 1 -> L2
//Prototype: vectorNorm(float vector inputVector, str mode), returns float norm.

float vectorNorm( std::vector<float>& inVector, int mode = 0){

	int size = inVector.size();

	float norm = 0.0;

	switch( mode ){

		case 0: {	//L1 (absolute norm)	
					for (int i = 0; i < size; ++i) 
					{
						norm = norm + std::abs(inVector[i]);
					}

					break;
				}

		case 1: {	 //L2 norm	
					for (int i = 0; i < size; ++i) 
					{
						norm = norm + std::abs(inVector[i]);
					}

					norm = sqrt(norm);

					break;
				}

	}

	return norm;

}

//function that receives a float vector and computes its normalized version.
//additionaly, a type of norm can be specified:
// mode = 0 -> L1, mode = 1 -> L2
//Prototype: normalizeVector(float vector inputVector, int mode = 0), 
//returns float vector normalizedVector.

std::vector<float> normalizeVector( std::vector<float>& inVector, int mode = 0 ){

	//first, compute vector's absolute norm:
	float norm = vectorNorm(inVector, mode);
	//get size of input vector:
	int size = inVector.size();
	//results go here:
	std::vector<float> outVector( size, 0.0 );

	for (int i = 0; i < size; ++i)
	{
		outVector[i] = std::abs( inVector[i]/norm );
	}

	return outVector;

}

//function that receives two vectors of features and computes euclideanDistance
//between them, according to some weights.
//Matlab: eucDistance =  sqrt(sum(w.*(X - Y).^ 2));
//Prototype: weightedEuclideanDistance(float vector referenceVector,
//float vector estimatedVector, float vector weightsVector), 
//returns float eucDistance.

float weightedEuclideanDistance( std::vector<float>& referenceVector, std::vector<float>& estimatedVector, std::vector<float>& weightsVector ){

	//get size of input vectors:
	int referenceSize = referenceVector.size();
	int estimatedSize = estimatedVector.size();
	int weightsSize = weightsVector.size();

	//check for matching lenght:
	if( (referenceSize != estimatedSize) || (referenceSize != weightsSize) )
	//if ( (referenceSize != estimatedSize != weightsSize) )
	{
        std::cout<<"ref: "<<referenceSize<<std::endl;
        std::cout<<"est: "<<estimatedSize<<std::endl;
        std::cout<<"w: "<<weightsSize<<std::endl;
        std::cout<<"weightedEuclideanSimilarity>> input vectors size does not match."<<std::endl;
        return -1;
	}

	float referenceSum, estimatedSum = 0.0;
	//result is stored here:
	float eucDistance = 0.0;
	
	for (int i = 0; i < referenceSize; ++i)
	{
		eucDistance = eucDistance + weightsVector[i] * pow( referenceVector[i]-estimatedVector[i], 2 );
		referenceSum = referenceSum + referenceVector[i];
		estimatedSum = estimatedSum + estimatedVector[i];
	}

    //std::cout<< "referenceSum: " << std::setprecision(5) << referenceSum << std::endl;
    //std::cout<< "estimatedSum: " << std::setprecision(5) << estimatedSum << std::endl;

	//sqrt the accumulation and compute the final result:
	eucDistance = sqrt(eucDistance);

	return eucDistance;

}

//function that receives a number and clamps it to a defined range.
//Prototype: float clamp(float number, float lowerValue, float upperValue), 
//receives a float number, a float max value and float min value,
//returns float clamped number.

float clamp(float number, float lowerValue, float upperValue) {
  return std::max(lowerValue, std::min(number, upperValue));
}

//function that computes the (weighted) cosine similarity between 2 vectors.
//Prototype: float weightedCosineSimilarity(float reference vector, float estimated vector, float weights vector), 
//returns float cosine similarity that ranges [-1, 1]:
//similar vectors are > 0, different vectors are < 0

float weightedCosineSimilarity( std::vector<float>& referenceVector, std::vector<float>& estimatedVector, std::vector<float>& weightsVector ){

	//get size of input vectors:
	int referenceSize = referenceVector.size();

	std::vector<float> weightedReferenceVector(referenceSize,0);
	std::vector<float> weightedEstimatedVector(referenceSize,0);

	//weight the vectors:
	for (int i = 0; i < referenceSize; ++i)
	{
		weightedReferenceVector[i] = weightsVector[i]*referenceVector[i];
		weightedEstimatedVector[i] = weightsVector[i]*estimatedVector[i];
        //std::cout<<"weightedReferenceVector "<<i<<" :"<<weightedReferenceVector[i]<<std::endl;
        //std::cout<<"weightedEstimatedVector "<<i<<" :"<<weightedEstimatedVector[i]<<std::endl;
	}
	//get the norms using L2:
	float referenceNorm = vectorNorm( weightedReferenceVector, 1 );
	float estimatedNorm = vectorNorm( weightedEstimatedVector, 1 );
	float inverseDenominator = 1/(referenceNorm*estimatedNorm);	

    //std::cout<<"referenceNorm: "<<referenceNorm<<std::endl;
    //std::cout<<"estimatedNorm: "<<estimatedNorm<<std::endl;

	//normalize the vectors & compute dot product:
	float cosineSim = 0;
	for (int i = 0; i < referenceSize; ++i)
	{
		cosineSim = cosineSim + weightedReferenceVector[i]*weightedEstimatedVector[i]*inverseDenominator;
        //std::cout<<"cosineSim(Acc) "<<i<<" :"<<cosineSim<<std::endl;
	}

	return cosineSim;
}

//function that computes the median of a  vector.
//Prototype: float vecorMedian(float reference vector), 
//returns float median.

float vectorMedian( std::vector<float>& inVector ){

    int vectorSize = inVector.size();

    float median = -1;

    std::sort( inVector.begin(), inVector.end() );

    if ( vectorSize % 2 == 0 ){
        median = (inVector[vectorSize/2 - 1] + inVector[vectorSize/2]) / 2;
    }        
    else{
        median = inVector[vectorSize/2];
    }

    return median;

}

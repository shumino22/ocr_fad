/* 	File		: 	mlfunctions.h
    Version 	:	1.6
    Description	: 	Protoypes of Auxiliary functions for machine learning algorithms
    Date:		:	Jul 03, 2018
    Author		:	Ricardo Acevedo

*/

#ifndef MLFUNCTIONS_H
#define MLFUNCTIONS_H

float computeCompactness( float perimeter, float area );
//std::map<std::string, functionPointer> initDictionary();
float getExtraProperties(const std::string& pointerFunction, float arg1, float arg2);
float vectorNorm( std::vector<float>& inVector, int mode = 0);
std::vector<float> normalizeVector( std::vector<float>& inVector, int mode = 0 );
float weightedEuclideanDistance( std::vector<float>& referenceVector, std::vector<float>& estimatedVector, std::vector<float>& weightsVector );
float clamp(float number, float lowerValue, float upperValue);
float weightedCosineSimilarity( std::vector<float>& referenceVector, std::vector<float>& estimatedVector, std::vector<float>& weightsVector );
float vectorMedian( std::vector<float>& inVector );

#endif // MLFUNCTIONS_H

/*  File        :   idClassifier.cpp
    Version     :   1.0
    Description :   ID Classifier
    Date:       :   Jun 25, 2018
    Author      :   Ricardo Acevedo

*/

#include <iostream>
#include <string>
#include "ipfunctions.h"
#include "mlfunctions.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>

//structure of id classifier results:
struct idClassifierResults
{
    std::string idClass;
    cv::Rect resultBox;
    cv::Mat resultImg;
};

idClassifierResults idClassifier( cv::Mat inputImage, std::string filePath, bool verbose = false )
{

    if( !filePath.compare("-")  ){
        throw  "Where's my image file path, my dude?";
    }

    //copies of the original, 640 x 480:
    cv::Mat originalImage = inputImage.clone();
    cv::Mat completeImage = inputImage.clone();

    static const int resizedWidth = 320;
    static const int resizedHeight = 240;

    int inputWidth = inputImage.cols;
    int inputHeight = inputImage.rows;

    int resizeFactor = inputWidth/resizedWidth;

    //resize the image:
    cv::resize(inputImage, inputImage, cv::Size(resizedWidth, resizedHeight), 0, 0, cv::INTER_AREA);

    //prepare segmented mats:
    cv::Mat idHeader;

    //define the header roi rectangle:
    cv::Rect headerROI;
    headerROI.x = 39;
    headerROI.y = 44;
    headerROI.width = 239;
    headerROI.height = 42;

    //crop the header:
    idHeader = inputImage(headerROI);


    //resize (up) only the header, just a little bit:
    cv::resize( idHeader, idHeader, cv::Size(), 1.5, 1.5, cv::INTER_LINEAR );

    //header thresholding:
    cv::Mat grayHeader = idHeader;
    cv::cvtColor(idHeader, grayHeader, cv::COLOR_RGB2GRAY);


    //apply header gaussian thresholding with parameters:
    cv::adaptiveThreshold(grayHeader,grayHeader,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY_INV,21,13);

    //read templates:
    cv::Mat template01 = cv::imread( filePath + "/nacional_template.png" );
    cv::Mat template02 = cv::imread( filePath + "/federal_template.png" );
    cv::Mat template03 = cv::imread( filePath + "/federaid_template.png" );

    if( ( template01.empty() || template02.empty() || template03.empty() ) ){

        throw "Problem loading templates. (Check image path)";

    }


    //vector of template mats:
    std::vector<cv::Mat> templateVector;
    templateVector.push_back(template01);
    templateVector.push_back(template02);
    templateVector.push_back(template03);

    //vector of types:
    std::vector<std::string> templateType;
    templateType.push_back("01 (INE)");
    templateType.push_back("02 (IFE)");
    templateType.push_back("03 (IFE)");

    //vector of sub-types:
    std::vector<std::string> templateSubtype;
    templateSubtype.push_back("[E]");
    templateSubtype.push_back("-");
    templateSubtype.push_back("[D]");

    //select the matching method:
    static const int matchMethod = CV_TM_CCOEFF_NORMED;

    //bestMatch variable to hold historic best match
    double bestMatch = 0;
    //store the bestMatch X,Y point on the image
    cv::Point bestMatchPoint;
    std::string idType = "Unknown/No Data";
    std::string idSubType = "Unknown/No Data";

    int bestTemplateCols; int bestTemplateRows;

    //minimum allowable match value:
    static const float minMatchValue = 0.6;

    //begin template match processing:
    for (int i = 0; i < (int)templateVector.size(); ++i)
    {
        //get the current template:
        cv::Mat currentTemplate;

        currentTemplate = templateVector[i];

        //rgb2gray conversion:
        cv::cvtColor(currentTemplate, currentTemplate, cv::COLOR_RGB2GRAY);

        //mat used to store template match results:
        cv::Mat matchResult;
        int resultCols =  grayHeader.cols - currentTemplate.cols + 1;
        int resultRow = grayHeader.rows - currentTemplate.rows + 1;
        matchResult.create( resultRow, resultCols, CV_32FC1 );

        //apply the matching algorithm:
        cv::matchTemplate( grayHeader, currentTemplate, matchResult, matchMethod );

        double minVal; double maxVal; double matchVal;
        cv::Point minLoc; cv::Point maxLoc; cv::Point matchPoint;

        //get the location of the best match:
        cv::minMaxLoc( matchResult, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
        __android_log_print(ANDROID_LOG_INFO, "idClassifier", "minVal=%f", minVal);
        __android_log_print(ANDROID_LOG_INFO, "idClassifier", "maxVal=%f", maxVal);
        __android_log_print(ANDROID_LOG_INFO, "idClassifier", "minLoc x=%d y=%d", minLoc.x, minLoc.y);
        __android_log_print(ANDROID_LOG_INFO, "idClassifier", "maxLoc x=%d, y=%d", maxLoc.x, maxLoc.y);

        //for the first two methods ( TM_SQDIFF and MT_SQDIFF_NORMED ) the best match are the lowest values.
        //for all the others, higher values represent better matches

        if( matchMethod  == cv::TM_SQDIFF || matchMethod == cv::TM_SQDIFF_NORMED )
        {
            matchPoint = minLoc;
            matchVal = 1-minVal;

        }else{
            matchPoint = maxLoc;
            matchVal = maxVal;
        }

        //let's see if the match value is above minimum and if it is the best match so far:
        if ( (matchVal > minMatchValue) && (matchVal > bestMatch) ){

            //store all the best match values:
            bestMatch = matchVal;
            bestMatchPoint = matchPoint;
            idType = templateType[i];
            idSubType = templateSubtype[i];

            bestTemplateCols = currentTemplate.cols;
            bestTemplateRows = currentTemplate.rows;

        }

    }

    //prepare body mat:
    cv::Mat idBody( cv::Size(640, 480), CV_8UC3, cv::Scalar(0) );
    cv::Rect bodyROI;
    //show results, if any, above a minimum threshold:

    if ( bestMatch >= minMatchValue ){

        //define the body roi rectangle:
        cv::rectangle( idHeader, bestMatchPoint, cv::Point( bestMatchPoint.x + bestTemplateCols , bestMatchPoint.y + bestTemplateRows), cv::Scalar(0,0,255), 1, 8, 0 );


        bodyROI.x = resizeFactor*39 + 5; //x  position + some threshold
        bodyROI.y = resizeFactor*(headerROI.y + headerROI.height);
        bodyROI.width = resizeFactor*239;
        bodyROI.height = resizeFactor*194 - bodyROI.y;

        //crop the body:
        idBody = originalImage(bodyROI);

        //if ID type == "02 (IFE)" search for "curp" text:

        if ( !idType.compare("02 (IFE)") ){

            //curp roi:
            cv::Rect curpROI;
            curpROI.x = 0;
            curpROI.height = floor(0.34 * idBody.rows);
            curpROI.y = idBody.rows - curpROI.height;
            curpROI.width = floor(0.12 * idBody.cols);

            cv::Mat curpArea = idBody(curpROI);

            //curp roi thresholding:
            cv::Mat grayCurp;
            cv::cvtColor(curpArea, grayCurp, cv::COLOR_RGB2GRAY);

            //apply the thresholding:
            cv::adaptiveThreshold(grayCurp,grayCurp,255,cv::ADAPTIVE_THRESH_MEAN_C,cv::THRESH_BINARY_INV,3,10);

            //run template matching:

            //read curp template:
            cv::Mat curpTemplate;
            curpTemplate = cv::imread(filePath + "curp_template.png");

            if( (curpTemplate.empty() ) ){

                throw "IdClassifier>> Problem loading curp template. (Check image path)";

            }


            //RGB2Gray conversion:
            cv::cvtColor(curpTemplate, curpTemplate, cv::COLOR_RGB2GRAY);

            //mat used to store template match results:
            cv::Mat curpMatchResult;
            int resultCols =  grayCurp.cols - curpTemplate.cols + 1;
            int resultRow = grayCurp.rows - curpTemplate.rows + 1;
            curpMatchResult.create( resultRow, resultCols, CV_32FC1 );

            //run template matching:
            cv::matchTemplate( grayCurp, curpTemplate, curpMatchResult, matchMethod );

            double minVal; double maxVal; double matchVal;
            cv::Point minLoc; cv::Point maxLoc; cv::Point matchPoint;

            //find point of maximum correlation
            cv::minMaxLoc( curpMatchResult, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );

            //for the first two methods ( TM_SQDIFF and MT_SQDIFF_NORMED ) the best match are the lowest values.
            //for all the others, higher values represent better matches

            if( matchMethod  == cv::TM_SQDIFF || matchMethod == cv::TM_SQDIFF_NORMED )
            {
                matchPoint = minLoc;
                matchVal = 1-minVal;

            }else{
                matchPoint = maxLoc;
                matchVal = maxVal;
            }

            static const float minMatchValue = 0.5;

            //let's see if something above the min value is found:
            if ( (matchVal >= minMatchValue) ){
                //found CURP matching:
                idSubType = "CURP [C]";

                cv::cvtColor(grayCurp, grayCurp, cv::COLOR_GRAY2BGR);
                cv::rectangle( grayCurp, matchPoint, cv::Point( matchPoint.x + curpTemplate.cols , matchPoint.y + curpTemplate.rows), cv::Scalar(0,0,255), 1, 8, 0 );
                //show results:
            }
        }
    }

    //show final results:
    cv::Rect finalCrop;
    finalCrop.width = 2*headerROI.width;
    finalCrop.height =  (2*headerROI.height + bodyROI.height);
    finalCrop.x = 2*headerROI.x;
    finalCrop.y = 2*headerROI.y;

    //crop final image:

    completeImage = completeImage(finalCrop);

    //prepare a black textbox:
    cv::Rect textBox;

    textBox.width = 135;
    textBox.height = 15;

    //idBody is only the body, completeImage is complete image

    textBox.x = completeImage.cols - textBox.width;
    textBox.y = completeImage.rows - textBox.height;

    std::string finalResult = idType+" "+idSubType;

    std::cout<<"\n"<<"Id Classifier>> Input is: "<<finalResult<<std::endl;
    //draw a black textbox:
    cv::rectangle( completeImage,textBox,cv::Scalar(0),CV_FILLED,cv::LINE_8,0);
    //put text (id type + subtype):
    cv::putText( completeImage, finalResult, cv::Point(textBox.x+1,textBox.y+11),cv::FONT_HERSHEY_SIMPLEX,0.4,cv::Scalar(0,255,0),1,cv::LINE_8,false );

    //show me the goods:

    //return the final results, packed in the results struct:
    idClassifierResults functionResults;
    functionResults.idClass = finalResult;
    functionResults.resultBox = textBox;
    functionResults.resultImg = completeImage;

    return functionResults;
}

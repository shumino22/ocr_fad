/* 	File		: 	ipfunctions.h
    Version 	:	1.5
    Description	: 	Protoypes of Auxiliary functions for image processing algorithms
    Date:		:	Jul 03, 2018
    Author		:	Ricardo Acevedo

*/

#ifndef IPFUNCTIONS_H
#define IPFUNCTIONS_H

#include <opencv2/opencv.hpp>

// RGB pixel definition:
typedef cv::Point3_<uint8_t> rgbPixel;
void setVerbose( bool verbose = false );

struct boundingBox
{
   int x;
   int y;
   int width;
   int height;
};

void computeBoundingBox( cv::Mat bwImage, boundingBox &boxData );

int countHoles( cv::Mat bwImage );

//structure of countConnectedComponents' function (can be 2 types):
struct ccResults
{
   int cc; //number of connected components:
   cv::Mat filteredMat; //filtered Mat;
};

ccResults countConnectedComponents( cv::Mat bwImage, int connectivity = 8, bool filterBlob = false, int minArea = 0 );
int eulerNumber( cv::Mat bwImage, int connectivity = 8, bool filterBlob = false, int minArea = 0 );
cv::Mat hitOrMiss( cv::Mat bwImage );
cv::Mat skeletonize( cv::Mat bwImage );
//void extraStatistics( cv::Mat bwImage, bool isImage = true );

struct properties
{
    float property01;
    float property02;
    float property03;
    float property04;
    float property05;
};

properties getExtraProperties( const std::string& pointerFunction, cv::Mat bwImage, float arg1 = 0, float arg2 = 0 );

void getCrushedVector( cv::Mat inputImage, std::vector<uchar>& crushedVector, int maskStartX = 0, int maskEndX = 0, int maskStartY = 0, int scanRows = 0 );
void rgbNormalized( rgbPixel &rgbPixel, const int *position );
void rgbNormalizedNaive(cv::Mat &inputImage);

void contrastBrightnessAdjustment( cv::Mat inputImage, float alpha, int beta );
double getPSNR( cv::Mat I1);

#endif // IPFUNCTIONS_H

/*  File        :   idlocator.cpp
    Version     :   1.5
    Description :   ID Locator Functionality
    Date:       :   Jun 13, 2018
    Author      :   Ricardo Acevedo

*/

#include <iostream>
#include <vector>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include "ipfunctions.h" //image processing auxiliary functions
#include "mlfunctions.h" //machine learning auxiliary functions

// RGB pixel definition:
typedef cv::Point3_<uint8_t> rgbPixel;


float idLocator( cv::Mat inputImage, bool verbose = false ){

    bool imgCheck = inputImage.isContinuous();
    //prepare the gray and the Normalized RGB
    int imgChannels = inputImage.channels();
    //version of the input image:
    cv::Mat normalizedRGB, grayImage;

    //RGB 2 gray conversion:
    //cv::cvtColor(inputImage, grayImage, cv::COLOR_BGR2GRAY);

    //deep copy of input image:
    normalizedRGB = inputImage.clone();

    //accelerated normalized RGB conversion:
    normalizedRGB.forEach<rgbPixel>(&rgbNormalized);

    std::vector<cv::Mat> redChannel;   //destination array
    //extrar red channel:
    cv::split( normalizedRGB,redChannel );//split source

    //cv::cvtColor(redChannel[0], redChannel[0], cv::COLOR_BGR2GRAY);

    //cv::GaussianBlur( normalizedRGB, normalizedRGB, cv::Size( 9, 9 ), 0, 0 );

    //show me the images:
    //if (verbose){

    //cv::namedWindow("Gray Image");
    //cv::imshow( "Gray Image", grayImage );

    //cv::namedWindow("Gaussian Filter");
    //cv::imshow( "Gaussian Filter", normalizedRGB );
    // }

    //scalars for gray mean and gray stdDev:
    cv::Scalar tempMean, tempStdDev;

    //compute gray mean/stdDev of the gray input image:
    cv::meanStdDev(redChannel[0], tempMean, tempStdDev);

    float redVarCoeff = pow( tempStdDev[0],2 )/tempMean[0];

    //show the computed statics:
    //define marker (quad) centers:
    std::vector<cv::Point> markerCenters;
    markerCenters.push_back( cv::Point(39,44) );
    markerCenters.push_back( cv::Point(279,44) );
    markerCenters.push_back( cv::Point(39,194) );
    markerCenters.push_back( cv::Point(279,194) );

    //Subsample window dimensions definition:
    static const int sampleWinHeight = 35;
    static const int sampleWinWidth = 35;

    //define sample quads coordinates:
    std::vector<cv::Point> sampleQuadCoordinates;
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[0].x - sampleWinWidth, markerCenters[0].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[1].x, markerCenters[1].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[2].x - sampleWinWidth, markerCenters[2].y) );
    sampleQuadCoordinates.push_back( cv::Point(markerCenters[3].x, markerCenters[3].y) );

    //Classification dimensions window:
    static const int classWinHeight = 15;
    static const int classWinWidth = 15;

    //define classification quads coordinates:
    std::vector<cv::Point> classificationQuadCoordinates;
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[0].x, markerCenters[0].y) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[1].x - classWinWidth, markerCenters[1].y) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[2].x, markerCenters[2].y - classWinHeight) );
    classificationQuadCoordinates.push_back( cv::Point(markerCenters[3].x - classWinWidth, markerCenters[3].y - classWinHeight) );

    //total quads on image:
    static const int totalQuads = 4;
    //decimate factor for sampling:
    static const int sampleDecimateFactor = 1;

    //std devs for each color are stored here, this is a flat vector.
    //I neeed this format because I'll compute the median of all channels at once:
    std::vector<float> stdVector ( 3*totalQuads, 0 );
    //std::vector<float> stdVectorAverage ( totalQuads, 0 );
    std::vector<float> meanVector ( 3*totalQuads, 0 );

    //RBG vartiation coeficients for each quad are stored here:
    std::vector<float> varCoeffVector ( 3*totalQuads, 0 );

    //valid pixels per quad go in this vector:
    std::vector<float> validPixelsVector ( 3*totalQuads, 0 );

    //scalars for image mean and image stdDev:
    cv::Scalar imageMean, imageStdDev;

    //sample window RGB matrix:
    cv::Mat sampleWindow = sampleWindow.zeros(sampleWinHeight, sampleWinWidth, CV_8UC3);

    //the quad counter:
    int quadCount = 0;

    /*
    **************
    Quad Sampling
    **************
    */

    //Loop thru all 4 defined quads:
    for ( int currentQuad = quadCount; currentQuad < totalQuads; ++currentQuad )
    {

        //start of the current quad in x:
        int quadColStart = sampleQuadCoordinates[currentQuad].x;
        //end of the current quad in x:
        int quadColEnd = quadColStart + sampleWinWidth;

        //start of the current quad in y:
        int quadRowStart = sampleQuadCoordinates[currentQuad].y;
        //end of the current quad in y:
        int quadRowEnd = quadRowStart + sampleWinHeight;

        //row index for target image (sampleWindow):
        int rowIndex = 0;

        //loop thru each quad's row:
        int rowEnd = quadRowEnd - sampleDecimateFactor;
        for ( int j = quadRowStart; j <= rowEnd; j = j + sampleDecimateFactor )
        {
            //col index for target image (sampleWindow):
            int colIndex = 0;

            //since this is a pixel-address approach, I need the current pixel of the sampled image
            //and the target pixel in the sample window.
            //I'll create a new "image" consisting of the sampled portion of the image
            //(the sample window):
            cv::Vec3b* currentPixel = normalizedRGB.ptr<cv::Vec3b>(j);
            cv::Vec3b* targetPixel = sampleWindow.ptr<cv::Vec3b>(rowIndex);

            //loop thru each quad's column:
            int colEnd = quadColEnd - sampleDecimateFactor;
            for ( int i = quadColStart; i <= colEnd; i = i + sampleDecimateFactor )
            {
                //fill the subwindow pixels with input image's
                targetPixel[colIndex] = currentPixel[i];
                colIndex++;
            }
            rowIndex++;
        }

        //compute and store statistics in vector:
        cv::meanStdDev(sampleWindow, imageMean, imageStdDev);

        // stdVectorIndex for storing my values:
        int stdVectorIndex = 3 * currentQuad;
        int decrementIndex = 2; // 3 colors: RGB

        //in each loop iteration, the vector stores 3 values,
        //loop thru each color (RGB):
        for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
        {
            //opencv operates RGB images as BGR (WTF?):
            stdVector[ stdVectorIndex + colorIndex ] =  imageStdDev[decrementIndex];
            meanVector[ stdVectorIndex + colorIndex ] = imageMean[decrementIndex];
            varCoeffVector[ stdVectorIndex + colorIndex ] = pow( imageStdDev[decrementIndex],2 )/imageMean[decrementIndex];

            decrementIndex--;
        }
    }

    static const int classDecimateFactor = 2;

    //target color of the object of interest:
    std::vector<int> targetColor{ 84, 84, 84 }; //rgb

    std::vector<int> markedPixelsVector (totalQuads,0);

    /*
    *******************
    Quad Classification
    *******************
    */
    int rgbSimilarBg;
    //Quad processing (Pixel Classification for each quad):
    for (int currentQuad = quadCount; currentQuad < totalQuads; ++currentQuad)
    {
        //start of the current quad in x:
        int quadColStart = classificationQuadCoordinates[currentQuad].x;
        //end of the current quad in x:
        int quadColEnd = quadColStart + classWinWidth;

        //start of the current quad in y:
        int quadRowStart = classificationQuadCoordinates[currentQuad].y;
        //end of the current quad in y:
        int quadRowEnd = quadRowStart + classWinHeight;

        int pixelCount = 0;

        //loop thru each quad's row:
        int rowEnd = quadRowEnd - classDecimateFactor;
        for ( int j = quadRowStart; j <= rowEnd; j = j + classDecimateFactor )
        {
            //read current pixel row:
            cv::Vec3b* currentPixel = normalizedRGB.ptr<cv::Vec3b>(j);

            //loop thru each quad's column:
            int colEnd = quadColEnd - classDecimateFactor;
            for ( int i = quadColStart; i <= colEnd; i = i + classDecimateFactor )
            {

                //rgb similarity counter:
                rgbSimilarBg = 0;

                //background and target same color check:
                for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
                {

                    int stdVectorIndex = 3*currentQuad + colorIndex;

                    //current mean:
                    float meanValue = floor( meanVector[stdVectorIndex] );

                    //differece between current mean and the target color:
                    float meanTargetDifference = abs( meanValue - targetColor[colorIndex] );
                    float varCoef = varCoeffVector[stdVectorIndex];
                    //checek against a minimum difference:
                    if (
                            ( (meanTargetDifference < 5) && (redVarCoeff > 0.05) )  //varCoef >= 0.005 condition 1: similar BG color to target
                        //|| ( (meanTargetDifference < 2) && (redVarCoeff >= 0.05) )                      //condition 2: very similar BG color to target
                            ) {
                        //if true, current color is similar to target color:
                        rgbSimilarBg++;
                    }
                }

                //std::cout<<"currentQuad: "<<currentQuad<<" rgbSimilarBg: "<<rgbSimilarBg<<std::endl;

                //RGB check value:
                int colorCheck = 0;

                //loop thru each color (RGB):
                for ( int colorIndex = 0; colorIndex < 3; ++colorIndex )
                {

                    int stdVectorIndex = 3*currentQuad + colorIndex;

                    //current variation coefficient:
                    float variationCoefficient = varCoeffVector[stdVectorIndex];
                    //Std dev Thresh:
                    float stdThresh;

                    //store number of valid pixels in vector
                    if (rgbSimilarBg == 3 ){
                        validPixelsVector[currentQuad] = 0.3;
                    }else{
                        validPixelsVector[currentQuad] = 0.4;
                    }

                    //Assing std thresh according to variation coefficient:
                    if ( (variationCoefficient > 1) || (rgbSimilarBg == 3) ){
                        //high dispersion found:
                        stdThresh = 0.5;
                    }
                    else{
                        stdThresh = 3.3;
                    }

                    //current statistics:
                    float meanValue = floor( meanVector[stdVectorIndex] );
                    float stdDevValue = stdThresh * stdVector[stdVectorIndex];
                    float meanUpperLimit = floor( meanValue + stdDevValue );
                    float meanLowerLimit = floor( meanValue - stdDevValue );

                    //current pixel (just one color) to int conversion:
                    float currentColor = (float)currentPixel[i][2-colorIndex];

                    //"voting" system
                    //each color "votes" if its value is outside the expected stats,
                    //3 is the "heaviest" weight
                    //if the current pixel falls outside the expected range:
                    if ( (currentColor <= meanLowerLimit) || (currentColor >= meanUpperLimit) )
                    {
                        int stdDevFactor;
                        if (stdThresh == 0.5){
                            stdDevFactor = 1.8;
                        }else{
                            stdDevFactor = 4;
                        }

                        //check out the difference between current color and mean:
                        float colorDifference = abs( currentColor - meanValue );
                        //maximum Std Dev value to be consider a new color:
                        float maxStdDevFactor = stdDevFactor * stdVector[stdVectorIndex];
                        //see if the difference is greater than the expected:
                        if ( colorDifference > maxStdDevFactor )
                        {
                            colorCheck = 3; //yes, there's difference, vote with the "heaviest" weight

                        }else{

                            //only count colors different from the target
                            //float targetColorDifference = abs( currentColor - targetColor[colorIndex] );
                            if ( colorDifference > 0 ){
                                colorCheck++; //just a weight of 1
                            }
                        }
                    }
                }

                //after checking the 3 channels, check how many colors where different:
                if ( colorCheck >= 3 )
                {
                    //lt's draw some info:
                    if(rgbSimilarBg >= 3){
                        //foreground object with a bg similar to target color:
                        cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(17,172,245) );
                    }
                    else{
                        //foreground object with a bg different from target color:
                        cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(0,0,255) );
                    }

                    //pixels matched so far (for each marker/quad):
                    pixelCount++;

                }else{
                    //no object detected:
                    cv::line( inputImage, cv::Point( i, j ), cv::Point( i, j ), cv::Scalar(255,0,0) );
                }
            }

        }

        //Store number of pixels checked
        //this vector stores the total "voters" per quad:
        markedPixelsVector[currentQuad] = pixelCount;
    }

    //Result analysis:

    //total of pixels analyzed per quad:
    float classWinPixels = classWinHeight * classWinWidth * 1/( pow(classDecimateFactor,2) );

    //check markedPixelsVector for valid foreground object:
    int markerCount = 0 ;

    bool q1 = false;
    bool q2 = false;
    bool q3 = false;
    bool q0 = false;

    for (int i = 0; i < totalQuads; ++i)
    {
        int redColor = 255; //not enough foreground pixels detected (red)
        int greenColor = 0; //enough foreground pixels detected (green)

        // percent of the pixels in window that must defined a "foreing object":
        float validPixels = floor( validPixelsVector[i] * classWinPixels );
        //std::cout<<"Quad: "<<i<<" Valid Pixels %: "<<validPixelsVector[i]<<" Valid Pixels: "<<validPixels<<std::endl;

        //check pixel votes, the number in the vector must equal
        //the number of valid pixels:
        if ( markedPixelsVector[i] >= validPixels ) //enough foreground pixels
        {
            //a foreing object is found in the picture
            markerCount++;
            redColor = 0;
            greenColor = 255;


            // no ID Function: determine if an object is found in the picture:
            switch (i)
            {
                case 0:
                    q0 = true;
                    break;
                case 1:
                    q1 = true;
                    break;
                case 2:
                    q2 = true;
                    break;
                case 3:
                    q3 = true;
                    break;
            }

        }
        //draw rectangle with results: green -> foreign object detected in quad, red-> no detection
        cv::rectangle( inputImage, classificationQuadCoordinates[i], classificationQuadCoordinates[i] + cv::Point(classWinWidth, classWinHeight), cv::Scalar(0,greenColor,redColor));
    }

    bool noIdFunction = (q0 && q3 && !q1 && !q2) || (q1 && q2 && !q0 && !q3) ;

    //this is the detection code that the module returns:
    //3 -> No ID present in the image
    //1 -> Aligned ID found
    //2 -> ID found but no aligned

    int detectionCode = 0;

    if ( markerCount == 4 ){
        detectionCode = 1;
    }else if ( (noIdFunction == 1) || (markerCount <= 1)  ){
        detectionCode = 3;
    }else{
        detectionCode = 2;
    }

    return detectionCode;
}
